module Main exposing (..)

import Browser
import Html exposing (Html, br, button, div, h1, input, span, text)
import Html.Attributes exposing (placeholder)
import Html.Events exposing (onClick, onInput)
import Http
import Json.Decode as Decode
import String exposing (fromFloat)
import Material
import Material.Button as Button
import Material.Card as Card
import Material.Elevation as Elevation
import Material.Options as Options exposing (cs, css, styled, when)
import Material.Textfield as Textfield
import Material.Typography as Typography
import Round exposing (..)



---- MODEL ----


type alias Model =
    { temperatureInfo : TemperatureInfo
    , city : String
    , mdc : Material.Model Msg
    }


type alias TemperatureInfo =
    { name : String
    , windSpeed : Float
    , temperature : Float
    , pressure : Float
    , humidity : Float
    }


init : ( Model, Cmd Msg )
init =
    ( Model (TemperatureInfo "Did not load" 0 0 0 0) "" Material.defaultModel
    , Cmd.none
    )



---- UPDATE ----


getTemperature city =
    let
        url =
            "http://api.openweathermap.org/data/2.5/weather?q=" ++ city ++ "&APPID=<your_app_id>"
    in
    Http.send NewTemp (Http.getString url)


decodeTemperatureInfo : String -> TemperatureInfo
decodeTemperatureInfo json =
    let
        name =
            Result.withDefault "Error decoding data!" (Decode.decodeString (Decode.field "name" Decode.string) json)

        windSpeed =
            Result.withDefault 0 (Decode.decodeString (Decode.at [ "wind", "speed" ] Decode.float) json)

        temperature =
            Result.withDefault 0 (Decode.decodeString (Decode.at [ "main", "temp" ] Decode.float) json) - 273.15

        pressure =
            Result.withDefault 0 (Decode.decodeString (Decode.at [ "main", "pressure" ] Decode.float) json)

        humidity =
            Result.withDefault 0 (Decode.decodeString (Decode.at [ "main", "humidity" ] Decode.float) json)
    in
    TemperatureInfo name windSpeed temperature pressure humidity


type Msg
    = GetTemp
    | CityInput String
    | NewTemp (Result Http.Error String)
    | Mdc (Material.Msg Msg)


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        Mdc msg_ ->
            Material.update Mdc msg_ model

        GetTemp ->
            ( model, getTemperature model.city )

        NewTemp (Ok json) ->
            let
                newTemperatureInfo =
                    decodeTemperatureInfo json
            in
            ( { model | temperatureInfo = newTemperatureInfo }, Cmd.none )

        NewTemp (Err _) ->
            ( model, Cmd.none )

        CityInput city ->
            ( { model | city = city }, Cmd.none )



---- VIEW ----


view : Model -> Html Msg
view model =
    div []
        [ Textfield.view Mdc
            "city-input"
            model.mdc
            [ Textfield.label "City"
            , Textfield.value model.city
            , Options.onInput CityInput
            ]
            []
        , Button.view Mdc
            "get-temp-btn"
            model.mdc
            [ Button.raised
            , Button.ripple
            , Options.onClick GetTemp
            ]
            [ text "Get temperature" ]
        , br [] []
        , Card.view
            [ css "width" "256px"
            , css "margin" "20px"
            , Elevation.z8
            , Elevation.transition
            , css "background-color" "rgba(173, 216, 230, 0.5)"
            ]
            [ Card.media
                [ css "flex-direction" "column" ]
                [ styled h1 [] [text model.city ]]
            ,styled div
                [ css "padding" "2rem 2rem 0 2rem"]
                [ styled span
                    [ Typography.display4
                    , css "contract" "0.87"
                    ]
                    [ text (Round.round 0 model.temperatureInfo.temperature ++ "°") ]
                ]
            , Card.actions []
                [ div [] [ text "Wind: ", text (fromFloat model.temperatureInfo.windSpeed) ]
                , div [] [ text "Pressure: ", text (fromFloat model.temperatureInfo.pressure) ]
                , div [] [ text "Humidity: ", text (fromFloat model.temperatureInfo.humidity) ]
                ]
            ]
        ]



-- Subscriptions


subscriptions : Model -> Sub Msg
subscriptions model =
    Sub.none



---- PROGRAM ----


main : Program () Model Msg
main =
    Browser.element
        { view = view
        , init = \_ -> init
        , update = update
        , subscriptions = subscriptions
        }
