module Main exposing (..)

import Html exposing (Html, text, div, h1, p)

main : Html msg
main =
    div [ ]
    [ h1 [] [ text "Elm is fun!" ]
    , p [] [ text "Let's learn some more!" ]
    ]
