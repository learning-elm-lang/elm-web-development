module Main exposing (Msg(..), init, main, update, view)

import Browser
import Html exposing (Html, button, div, h1, input, label, p, span, text)
import Html.Attributes exposing (class, for, id, placeholder, src, type_)
import Html.Events exposing (onClick, onInput)
import String exposing (fromFloat)


type alias Model =
    { convertedValue : Float
    , ratio : Float
    , unit1 : String
    , unit2 : String
    }


init : Model
init =
    { unit1 = "Kilometers"
    , unit2 = "Miles"
    , ratio = 1.608
    , convertedValue = 0.0
    }


type Msg
    = Swap
    | Convert String


update msg model =
    case msg of
        Swap ->
            { model | unit1 = model.unit2, unit2 = model.unit1, ratio = 1 / model.ratio }

        Convert newValue ->
            let
                floatValue =
                    Maybe.withDefault 0 (String.toFloat newValue)
            in
                { model | convertedValue = floatValue * model.ratio }


view model =
    div []
        [ div [ class "col-lg-offset-3 col-lg-6 mt5 pt5" ]
            [ h1 []
                [ text "Unit Converter App" ]
            , div [ class "input-group" ]
                [ span [ class "input-group-btn" ]
                    [ button [ class "btn btn-secondary", type_ "button" ]
                        [ text model.unit1 ]
                    ]
                , input [ onInput Convert, class "form-control", placeholder "Type a number to convert", type_ "text" ] []
                , span [ class "input-group-btn" ]
                    [ button [ onClick Swap, class "btn btn-primary", type_ "button" ] [ text "Switch" ]
                    ]
                ]
            , div [ class "mt5 pt5" ] [ text model.unit2 ]
            , div [ id "unit2Value" ] [ text (fromFloat model.convertedValue) ]
            ]
        ]


main =
    Browser.sandbox
        { view = view
        , init = init
        , update = update
        }
