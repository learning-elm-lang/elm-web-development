module Main exposing (..)

import Debug exposing (toString)
import Html exposing (Html, text)
import String exposing (fromFloat, fromInt)


num = 100
modulusTest = (modBy 3 num, modBy 5 num)

raceTimes = { first = 60, second = 65, third = 71, fourth = 75, fifth = 90 }
averageRunnerTime record =
    let
        {first, second, third, fourth, fifth} = record
    in
        (first + second + third + fourth + fifth) / 5

extractorFunction { fifth } =
    fifth

extractorFunction2 { first, fifth } =
    (first, fifth)

main : Html msg
main =
    text <|
        let
            fizzBuzz n =
                case modulusTest of
                    ( 0, 0 ) -> "fizzBuzz"

                    ( 0, _ ) -> "fizz"

                    ( _, 0 ) -> "buzz"

                    ( _, _ ) -> fromInt n
        in
        fizzBuzz num

        ++ " | " ++ (fromFloat (averageRunnerTime raceTimes))
        ++ " | " ++ (fromFloat (extractorFunction raceTimes))
        ++ " | " ++ (fromFloat raceTimes.fourth)
        ++ " | " ++ (toString (extractorFunction2 raceTimes))
