module Main exposing (..)

import Browser
import Html exposing (Html, button, div, input, span, text)
import Html.Attributes exposing (class, placeholder, type_)
import Html.Events exposing (onInput)

-- Model
type alias Model =
    { inputValue: String
    , outputValue: String
    }

init : Model
init =
    { inputValue = ""
    , outputValue = ""
    }


-- Update
type Msg
    = DisplayInput String


update : Msg -> Model -> Model
update msg model =
    case msg of
        DisplayInput newValue ->
            let
                condition =
                    if modBy 15 (Maybe.withDefault 1 (String.toInt newValue)) == 0 then
                        "fizzBuzz"
                    else if modBy 5 (Maybe.withDefault 1 (String.toInt newValue)) == 0 then
                        "buzz"
                    else if modBy 3 (Maybe.withDefault 1 (String.toInt newValue)) == 0 then
                        "fizz"
                    else if (Maybe.withDefault 0 (String.toInt newValue)) /= 0 then
                        newValue
                    else
                        "Type a number, please"
            in
                { model | outputValue = condition }


-- View
view : Model -> Html Msg
view model =
    div []
        [ div [ class "col-lg-6" ]
            [ div [ class "input-group" ]
                [ input
                    [ onInput DisplayInput, class "form-control", placeholder "Enter a number", type_ "text" ]
                    []
                , span [ class "input-group-btn" ]
                    [ button [ class "btn btn-secondary", type_ "button" ]
                        [ text "FizzBuzz It!"]
                    ]
                ]
            , div [ class "display-4" ] [ text model.outputValue ]
            ]
        ]


-- Main
main =
    Browser.sandbox
    { init = init
    , view = view
    , update = update
    }
