module Main exposing (..)

import Browser
import Html exposing (Html, a, div, h1, h3, header, nav, node, text)
import Html.Attributes exposing (attribute, class, href, style)
import UnitConverter


bootstrapCss : Html.Html msg
bootstrapCss =
    let
        tag =
            "link"

        attrs =
            [ attribute "rel" "stylesheet"
            , attribute "property" "stylesheet"
            , attribute "href" "//maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.3/css/bootstrap.min.css"
            ]

        children =
            []
    in
    node tag attrs children


coverTemplateCss : Html.Html msg
coverTemplateCss =
    let
        tag =
            "link"

        attrs =
            [ attribute "rel" "stylesheet"
            , attribute "property" "stylesheet"
            , attribute "href" "//rawgit.com/twbs/bootstrap/v4-dev/docs/4.0/examples/cover/cover.css"
            ]

        children =
            []
    in
    node tag attrs children



---- MODEL ----


type alias Model =
    { lengthConverter : UnitConverter.Model
    , weightConverter : UnitConverter.Model
    }


init : Model
init =
    { lengthConverter = UnitConverter.init "Miles" "Kilometers" 1.608
    , weightConverter = UnitConverter.init "Kilograms" "Pounds" 2.2046
    }



---- UPDATE ----


type Msg
    = LengthConverterMsg UnitConverter.Msg
    | WeightConverterMsg UnitConverter.Msg


update : Msg -> Model -> Model
update msg model =
    case msg of
        LengthConverterMsg msg_ ->
            let
                newLengthConverter =
                    UnitConverter.update msg_ model.lengthConverter
            in
            { model | lengthConverter = newLengthConverter }

        WeightConverterMsg msg_ ->
            let
                newWeightConverter =
                    UnitConverter.update msg_ model.weightConverter
            in
            { model | weightConverter = newWeightConverter }



---- VIEW ----


view : Model -> Html Msg
view model =
    div
        [ style "background" "#0575e6"
        , style "background" "-webkit-linear-gradient(to right, #0575e6, #021b79)"
        , style "background" "linear-gradient(to right, #0575e6, #021b79)"
        , class "site-wrapper"
        ]
        [ bootstrapCss
        , coverTemplateCss
        , div [ class "site-wrapper-inner" ]
            [ div [ class "cover-container" ]
                [ header [ class "masthead clearfix" ]
                    [ div [ class "inner" ]
                        [ h3 [ class "masthead-brand" ]
                            [ text "Unit Converter Site" ]
                        , nav [ class "nav nav-masthead" ]
                            [ a [ class "nav-link text-secondary", href "#" ]
                                [ text "Home" ]
                            , a [ class "nav-link text-secondary", href "#" ]
                                [ text "Features" ]
                            , a [ class "nav-link text-secondary", href "#" ]
                                [ text "Contact" ]
                            ]
                        ]
                    ]
                , h1 [ class "cover-heading" ]
                    [ text "Distance and Weight converter" ]
                , UnitConverter.view model.lengthConverter |> Html.map LengthConverterMsg
                , UnitConverter.view model.weightConverter |> Html.map WeightConverterMsg
                ]
            ]
        ]



---- PROGRAM ----


main : Program () Model Msg
main =
    Browser.sandbox
        { view = view
        , init = init
        , update = update
        }
