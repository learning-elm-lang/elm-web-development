module UnitConverter exposing (..)

import Html exposing (Html, button, div, input, text)
import Html.Attributes exposing (attribute, class, id, placeholder, type_)
import Html.Events exposing (onClick, onInput)
import String exposing (fromFloat)


type alias Model =
    { unit1 : String
    , unit2 : String
    , ratio : Float
    , convertedValue : Float
    }


init : String -> String -> Float -> Model
init unit1 unit2 ratio =
    Model unit1 unit2 ratio ratio


view : Model -> Html Msg
view model =
    div [ class "site-wrapper" ]
        [ div [ class "inner cover" ]
            [ div [ class "col-lg-12 mb-5" ]
                [ div [ class "input-group mb-3" ]
                    [ div [ class "input-group-prepend" ]
                        [ button [ class "btn btn-warning", type_ "button" ] [ text model.unit1 ]
                        ]
                    , input
                        [ attribute "aria-describedby" "basic-addon1"
                        , attribute "aria-label" ""
                        , class "form-control"
                        , placeholder "Enter a numerical value"
                        , type_ "text"
                        , onInput Convert
                        ]
                        []
                    , div [ class "input-group-append" ]
                        [ button [ onClick Swap, class "btn btn-warning", type_ "button" ] [ text model.unit2 ]
                        , button [ id "unit2Value", class "btn btn-warning", type_ "button" ] [ text (fromFloat model.convertedValue) ]
                        ]
                    ]
                ]
            ]
        ]


type Msg
    = Swap
    | Convert String


update : Msg -> Model -> Model
update msg model =
    case msg of
        Swap ->
            { model | unit1 = model.unit2, unit2 = model.unit1, ratio = 1 / model.ratio }

        Convert newValue ->
            let
                floatValue =
                    Maybe.withDefault 1 (String.toFloat newValue)
            in
            { model | convertedValue = floatValue * model.ratio }
