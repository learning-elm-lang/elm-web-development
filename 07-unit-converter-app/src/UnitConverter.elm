module UnitConverter exposing (Model, Msg(..), init, update, view)

import Html exposing (Html, button, div, input, label, text)
import Html.Attributes exposing (for, id)
import Html.Events exposing (onClick, onInput)
import String exposing (fromFloat)



---- MODEL ----


type alias Model =
    { unit1 : String
    , unit2 : String
    , ratio : Float
    , convertedValue : Float
    }


init : String -> String -> Float -> Model
init unit1 unit2 ratio =
    Model unit1 unit2 ratio ratio



---- UPDATE ----


type Msg
    = Swap
    | Convert String


update : Msg -> Model -> Model
update msg model =
    case msg of
        Swap ->
            { model | unit1 = model.unit2, unit2 = model.unit1, ratio = 1 / model.ratio }

        Convert newValue ->
            let
                floatValue =
                    Maybe.withDefault 0 (String.toFloat newValue)
            in
                { model | convertedValue = floatValue * model.ratio }



---- VIEW ----


view : Model -> Html Msg
view model =
    div []
        [ div []
            [ label [ for "unit1Input" ] [ text model.unit1 ]
            , input [ id "unit1Input", onInput Convert ] []
            , button [ onClick Swap ] [ text "Switch" ]
            , label [ for "unit2" ] [ text model.unit2 ]
            , div [ id "unit2Value" ] [ text (fromFloat model.convertedValue) ]
            ]
        ]
