module Main exposing (Model, Msg(..), init, main, update, view)

import Browser
import Html exposing (Html, div)
import UnitConverter


type alias Model =
    { lengthConverter : UnitConverter.Model
    , weightConverter : UnitConverter.Model
    }


init : Model
init =
    { lengthConverter = UnitConverter.init "Miles" "Kilometers" 1.608
    , weightConverter = UnitConverter.init "Kilograms" "Pounds" 2.2046
    }


type Msg
    = LengthConverterMsg UnitConverter.Msg
    | WeightConverterMsg UnitConverter.Msg


view : Model -> Html Msg
view model =
    div []
        [ UnitConverter.view model.lengthConverter |> Html.map LengthConverterMsg
        , UnitConverter.view model.weightConverter |> Html.map WeightConverterMsg
        ]


update : Msg -> Model -> Model
update msg model =
    case msg of
        LengthConverterMsg msg_ ->
            let
                newLengthConverter =
                    UnitConverter.update msg_ model.lengthConverter
            in
                { model | lengthConverter = newLengthConverter }

        WeightConverterMsg msg_ ->
            let
                newWeightConverter =
                    UnitConverter.update msg_ model.weightConverter
            in
                { model | weightConverter = newWeightConverter }


main : Program () Model Msg
main =
    Browser.sandbox
        { view = view
        , init = init
        , update = update
        }
