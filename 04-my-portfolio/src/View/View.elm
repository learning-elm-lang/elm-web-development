module View.View exposing (view)

import Html exposing (Html, div, h1, p, text)
import Html.Attributes exposing (class)
import View.About exposing (about)
import View.Contact exposing (contact)
import View.Navigation exposing (navigation)
import View.Poem exposing (poem)
import View.Stories exposing (stories)


view : Html Never
view =
    div [ class "container-fluid" ]
        [ div [ class "container" ]
            [ h1 [] [ text "My Portfolio" ]
            , p [] [ text "Just Another Writer's Portfolio" ]
            , navigation
            , about
            , div [ class "row" ]
                [ div [ class "col" ]
                    [ poem ]
                , div [ class "col" ]
                    [ poem ]
                , div [ class "col" ]
                    [ poem ]
                ]
            , stories
            , contact
            ]
        ]
