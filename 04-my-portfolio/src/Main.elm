module Main exposing (main)

import Html exposing (Html, div)
import Html.Attributes exposing (class, style)
import View.View exposing (view)


main : Html Never
main =
    div [ class "bg-ligth" ]
        [ view ]
