module Main exposing (..)

import Browser
import Html exposing (Html, text, div, h1, button)
import Html.Events exposing (onClick)
import String exposing (fromInt)

---- MODEL ----
type alias Model =
    Int

init : ( Model )
init =
    ( 5 )

-- MESSAGE
type Msg
    = Decrement
    | Reset

---- UPDATE ----
update msg model =
    case msg of
        Decrement ->
            if model >= 1 then model - 1 else 5

        Reset ->
            5

---- VIEW ----
view model =
    div []
        [ h1 [] [ text ("Fruit to eat: " ++ (fromInt model)) ]
        , button [ onClick Decrement ] [ text "Eat fruit" ]
        , button [ onClick Reset ] [ text "Reset counter" ]
        ]

---- PROGRAM ----
main : Program () Model Msg
main =
    Browser.sandbox
    { init = init
    , view = view
    , update = update
    }
